def add(a, b):
    """Compute the sum of two numbers.
    
    Usage example:
    >>> add(4.0, 2.0)
    6.0
    >>> add(1, 3)
    4
    >>> add(1, 3)
    5
    """
    return a + b

result = add(1,2)
print(result) # 3